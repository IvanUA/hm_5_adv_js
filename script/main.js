// При відкритті сторінки необхідно отримати з сервера список всіх користувачів 
// та загальний список публікацій. Для цього потрібно надіслати GET запит на наступні дві адреси:
// https://ajax.test-danit.com/api/json/users
// https://ajax.test-danit.com/api/json/posts

function getUsersFetch() {
  fetch("https://ajax.test-danit.com/api/json/users", {
      method: 'GET'
    })
    .then(response => response.json())
    .then(users => {
      const [...u] = users;
      console.log("List of USERS:", ...u);
      getPostsFetch(users)
    })
    .catch(error => {
      console.error("Помилка при отриманні списку користувачів:", error);
    })
}

getUsersFetch();

function getPostsFetch(users) {
  fetch("https://ajax.test-danit.com/api/json/posts", {
      method: 'GET'
    })
    .then(response => response.json())
    .then(publications => {
      // Проходимося по списку користувачів
      users.forEach(user => {
        const userPublications = publications.filter(pub => pub.userId === user.id);
        displayUserPublications(user, userPublications);
      });
      const [...b] = publications;
      console.log("List of POSTS:", ...b);
    })
    .catch(error => {
      console.error("Помилка при отриманні списку постів:", error);
    })
}

//Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.

function displayUserPublications(users, publications) {
  const postDivHtml = document.querySelector('.posts')
  const publicationsList = document.createElement("ul");

  publications.forEach(pub => {
    const pubItem = document.createElement("li");
    pubItem.classList.add("post")
    const deleteButton = document.createElement('button');
    deleteButton.classList.add("delete")
    deleteButton.innerHTML = "Delete post";
    pubItem.textContent = `${users.name}  ${users.email} TITLE: ${pub.title} Text: ${pub.body} `;
    pubItem.appendChild(deleteButton);
    publicationsList.appendChild(pubItem);
    const deleteButtonArray = [deleteButton];
    for (let i = 0; i < deleteButtonArray.length; i++) {
      const button = deleteButtonArray[i];

      button.addEventListener("click", () => {
        const post = button.closest('.post')
        deletePostFetch(post);
      })
    }
  });
  postDivHtml.appendChild(publicationsList);
}

function deletePostFetch(publications) {
  // const pubID = publications.id;
  // console.log(pubID);
  const url = `https://ajax.test-danit.com/api/json/posts/1`;

  fetch(url, {
      method: 'DELETE',
    })
    .then(response => {
      if (response.ok) {
        publications.remove();
        console.log('Post deleted successfully');
      } else {
        console.error('Failed to delete post');
      }
    })
    .catch(error => {
      console.error('Error:', error);
    });
}